// Code generated by go-swagger; DO NOT EDIT.

package gameservers

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"fmt"
	"io"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
	"github.com/go-openapi/validate"

	"gitlab.com/BIC_Dev/nitrado-service-v3-client/nsmodels"
)

// GameserverStopReader is a Reader for the GameserverStop structure.
type GameserverStopReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GameserverStopReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewGameserverStopOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	case 400:
		result := NewGameserverStopBadRequest()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	default:
		result := NewGameserverStopDefault(response.Code())
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		if response.Code()/100 == 2 {
			return result, nil
		}
		return nil, result
	}
}

// NewGameserverStopOK creates a GameserverStopOK with default headers values
func NewGameserverStopOK() *GameserverStopOK {
	return &GameserverStopOK{}
}

/* GameserverStopOK describes a response with status code 200, with default header values.

success response
*/
type GameserverStopOK struct {
	Payload *nsmodels.GameserverStopResponse
}

func (o *GameserverStopOK) Error() string {
	return fmt.Sprintf("[POST /gameservers/{gameserver_id}/stop][%d] gameserverStopOK  %+v", 200, o.Payload)
}
func (o *GameserverStopOK) GetPayload() *nsmodels.GameserverStopResponse {
	return o.Payload
}

func (o *GameserverStopOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(nsmodels.GameserverStopResponse)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGameserverStopBadRequest creates a GameserverStopBadRequest with default headers values
func NewGameserverStopBadRequest() *GameserverStopBadRequest {
	return &GameserverStopBadRequest{}
}

/* GameserverStopBadRequest describes a response with status code 400, with default header values.

bad request
*/
type GameserverStopBadRequest struct {
	Payload *nsmodels.Error
}

func (o *GameserverStopBadRequest) Error() string {
	return fmt.Sprintf("[POST /gameservers/{gameserver_id}/stop][%d] gameserverStopBadRequest  %+v", 400, o.Payload)
}
func (o *GameserverStopBadRequest) GetPayload() *nsmodels.Error {
	return o.Payload
}

func (o *GameserverStopBadRequest) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(nsmodels.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGameserverStopDefault creates a GameserverStopDefault with default headers values
func NewGameserverStopDefault(code int) *GameserverStopDefault {
	return &GameserverStopDefault{
		_statusCode: code,
	}
}

/* GameserverStopDefault describes a response with status code -1, with default header values.

generic error
*/
type GameserverStopDefault struct {
	_statusCode int

	Payload *nsmodels.Error
}

// Code gets the status code for the gameserver stop default response
func (o *GameserverStopDefault) Code() int {
	return o._statusCode
}

func (o *GameserverStopDefault) Error() string {
	return fmt.Sprintf("[POST /gameservers/{gameserver_id}/stop][%d] gameserverStop default  %+v", o._statusCode, o.Payload)
}
func (o *GameserverStopDefault) GetPayload() *nsmodels.Error {
	return o.Payload
}

func (o *GameserverStopDefault) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(nsmodels.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

/*GameserverStopBody gameserver stop body
swagger:model GameserverStopBody
*/
type GameserverStopBody struct {

	// Message to display in game when stopping server (unreliable)
	// Required: true
	Message *string `json:"message"`

	// Authentication token for Nitrado service
	// Required: true
	Token *nsmodels.Token `json:"token"`

	// User who requested to stop the server
	// Required: true
	User *nsmodels.User `json:"user"`
}

// Validate validates this gameserver stop body
func (o *GameserverStopBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateMessage(formats); err != nil {
		res = append(res, err)
	}

	if err := o.validateToken(formats); err != nil {
		res = append(res, err)
	}

	if err := o.validateUser(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *GameserverStopBody) validateMessage(formats strfmt.Registry) error {

	if err := validate.Required("body"+"."+"message", "body", o.Message); err != nil {
		return err
	}

	return nil
}

func (o *GameserverStopBody) validateToken(formats strfmt.Registry) error {

	if err := validate.Required("body"+"."+"token", "body", o.Token); err != nil {
		return err
	}

	if o.Token != nil {
		if err := o.Token.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("body" + "." + "token")
			}
			return err
		}
	}

	return nil
}

func (o *GameserverStopBody) validateUser(formats strfmt.Registry) error {

	if err := validate.Required("body"+"."+"user", "body", o.User); err != nil {
		return err
	}

	if o.User != nil {
		if err := o.User.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("body" + "." + "user")
			}
			return err
		}
	}

	return nil
}

// ContextValidate validate this gameserver stop body based on the context it is used
func (o *GameserverStopBody) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	var res []error

	if err := o.contextValidateToken(ctx, formats); err != nil {
		res = append(res, err)
	}

	if err := o.contextValidateUser(ctx, formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *GameserverStopBody) contextValidateToken(ctx context.Context, formats strfmt.Registry) error {

	if o.Token != nil {
		if err := o.Token.ContextValidate(ctx, formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("body" + "." + "token")
			}
			return err
		}
	}

	return nil
}

func (o *GameserverStopBody) contextValidateUser(ctx context.Context, formats strfmt.Registry) error {

	if o.User != nil {
		if err := o.User.ContextValidate(ctx, formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("body" + "." + "user")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *GameserverStopBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *GameserverStopBody) UnmarshalBinary(b []byte) error {
	var res GameserverStopBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}
