// Code generated by go-swagger; DO NOT EDIT.

package gameservers

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/strfmt"
)

// NewGameserverStopParams creates a new GameserverStopParams object,
// with the default timeout for this client.
//
// Default values are not hydrated, since defaults are normally applied by the API server side.
//
// To enforce default values in parameter, use SetDefaults or WithDefaults.
func NewGameserverStopParams() *GameserverStopParams {
	return &GameserverStopParams{
		timeout: cr.DefaultTimeout,
	}
}

// NewGameserverStopParamsWithTimeout creates a new GameserverStopParams object
// with the ability to set a timeout on a request.
func NewGameserverStopParamsWithTimeout(timeout time.Duration) *GameserverStopParams {
	return &GameserverStopParams{
		timeout: timeout,
	}
}

// NewGameserverStopParamsWithContext creates a new GameserverStopParams object
// with the ability to set a context for a request.
func NewGameserverStopParamsWithContext(ctx context.Context) *GameserverStopParams {
	return &GameserverStopParams{
		Context: ctx,
	}
}

// NewGameserverStopParamsWithHTTPClient creates a new GameserverStopParams object
// with the ability to set a custom HTTPClient for a request.
func NewGameserverStopParamsWithHTTPClient(client *http.Client) *GameserverStopParams {
	return &GameserverStopParams{
		HTTPClient: client,
	}
}

/* GameserverStopParams contains all the parameters to send to the API endpoint
   for the gameserver stop operation.

   Typically these are written to a http.Request.
*/
type GameserverStopParams struct {

	/* Body.

	   Stop gameserver request body
	*/
	Body GameserverStopBody

	/* GameserverID.

	   Gameserver ID
	*/
	GameserverID string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithDefaults hydrates default values in the gameserver stop params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *GameserverStopParams) WithDefaults() *GameserverStopParams {
	o.SetDefaults()
	return o
}

// SetDefaults hydrates default values in the gameserver stop params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *GameserverStopParams) SetDefaults() {
	// no default values defined for this parameter
}

// WithTimeout adds the timeout to the gameserver stop params
func (o *GameserverStopParams) WithTimeout(timeout time.Duration) *GameserverStopParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the gameserver stop params
func (o *GameserverStopParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the gameserver stop params
func (o *GameserverStopParams) WithContext(ctx context.Context) *GameserverStopParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the gameserver stop params
func (o *GameserverStopParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the gameserver stop params
func (o *GameserverStopParams) WithHTTPClient(client *http.Client) *GameserverStopParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the gameserver stop params
func (o *GameserverStopParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithBody adds the body to the gameserver stop params
func (o *GameserverStopParams) WithBody(body GameserverStopBody) *GameserverStopParams {
	o.SetBody(body)
	return o
}

// SetBody adds the body to the gameserver stop params
func (o *GameserverStopParams) SetBody(body GameserverStopBody) {
	o.Body = body
}

// WithGameserverID adds the gameserverID to the gameserver stop params
func (o *GameserverStopParams) WithGameserverID(gameserverID string) *GameserverStopParams {
	o.SetGameserverID(gameserverID)
	return o
}

// SetGameserverID adds the gameserverId to the gameserver stop params
func (o *GameserverStopParams) SetGameserverID(gameserverID string) {
	o.GameserverID = gameserverID
}

// WriteToRequest writes these params to a swagger request
func (o *GameserverStopParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error
	if err := r.SetBodyParam(o.Body); err != nil {
		return err
	}

	// path param gameserver_id
	if err := r.SetPathParam("gameserver_id", o.GameserverID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
