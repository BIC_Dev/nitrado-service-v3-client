// Code generated by go-swagger; DO NOT EDIT.

package players

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
)

// New creates a new players API client.
func New(transport runtime.ClientTransport, formats strfmt.Registry) ClientService {
	return &Client{transport: transport, formats: formats}
}

/*
Client for players API
*/
type Client struct {
	transport runtime.ClientTransport
	formats   strfmt.Registry
}

// ClientOption is the option for Client methods
type ClientOption func(*runtime.ClientOperation)

// ClientService is the interface for Client methods
type ClientService interface {
	PlayerBan(params *PlayerBanParams, authInfo runtime.ClientAuthInfoWriter, opts ...ClientOption) (*PlayerBanOK, error)

	PlayerSearch(params *PlayerSearchParams, authInfo runtime.ClientAuthInfoWriter, opts ...ClientOption) (*PlayerSearchOK, error)

	PlayerUnban(params *PlayerUnbanParams, authInfo runtime.ClientAuthInfoWriter, opts ...ClientOption) (*PlayerUnbanOK, error)

	SetTransport(transport runtime.ClientTransport)
}

/*
  PlayerBan bans player

  Ban a player across all provided servers.
*/
func (a *Client) PlayerBan(params *PlayerBanParams, authInfo runtime.ClientAuthInfoWriter, opts ...ClientOption) (*PlayerBanOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewPlayerBanParams()
	}
	op := &runtime.ClientOperation{
		ID:                 "playerBan",
		Method:             "POST",
		PathPattern:        "/players/ban",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &PlayerBanReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	}
	for _, opt := range opts {
		opt(op)
	}

	result, err := a.transport.Submit(op)
	if err != nil {
		return nil, err
	}
	success, ok := result.(*PlayerBanOK)
	if ok {
		return success, nil
	}
	// unexpected success response
	unexpectedSuccess := result.(*PlayerBanDefault)
	return nil, runtime.NewAPIError("unexpected success response: content available as default response in error", unexpectedSuccess, unexpectedSuccess.Code())
}

/*
  PlayerSearch searches players

  Search for a player across all provided servers.
*/
func (a *Client) PlayerSearch(params *PlayerSearchParams, authInfo runtime.ClientAuthInfoWriter, opts ...ClientOption) (*PlayerSearchOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewPlayerSearchParams()
	}
	op := &runtime.ClientOperation{
		ID:                 "playerSearch",
		Method:             "POST",
		PathPattern:        "/players/search",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &PlayerSearchReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	}
	for _, opt := range opts {
		opt(op)
	}

	result, err := a.transport.Submit(op)
	if err != nil {
		return nil, err
	}
	success, ok := result.(*PlayerSearchOK)
	if ok {
		return success, nil
	}
	// unexpected success response
	unexpectedSuccess := result.(*PlayerSearchDefault)
	return nil, runtime.NewAPIError("unexpected success response: content available as default response in error", unexpectedSuccess, unexpectedSuccess.Code())
}

/*
  PlayerUnban unbans player

  Unban a player across all provided servers.
*/
func (a *Client) PlayerUnban(params *PlayerUnbanParams, authInfo runtime.ClientAuthInfoWriter, opts ...ClientOption) (*PlayerUnbanOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewPlayerUnbanParams()
	}
	op := &runtime.ClientOperation{
		ID:                 "playerUnban",
		Method:             "POST",
		PathPattern:        "/players/unban",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &PlayerUnbanReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	}
	for _, opt := range opts {
		opt(op)
	}

	result, err := a.transport.Submit(op)
	if err != nil {
		return nil, err
	}
	success, ok := result.(*PlayerUnbanOK)
	if ok {
		return success, nil
	}
	// unexpected success response
	unexpectedSuccess := result.(*PlayerUnbanDefault)
	return nil, runtime.NewAPIError("unexpected success response: content available as default response in error", unexpectedSuccess, unexpectedSuccess.Code())
}

// SetTransport changes the transport on the client
func (a *Client) SetTransport(transport runtime.ClientTransport) {
	a.transport = transport
}
