swagger generate client -A nscv3 -c nsclient -m nsmodels -f ~/go/src/gitlab.com/BIC_Dev/nitrado-service-v3/docs/swagger.yml -t ~/go/src/gitlab.com/BIC_Dev/nitrado-service-v3-client
go mod init
go mod tidy
GOPROXY=https://proxy.golang.org GO111MODULE=on go get gitlab.com/BIC_Dev/nitrado-service-v3-client@v1.0.0
curl https://sum.golang.org/lookup/gitlab.com/BIC_Dev/nitrado-api-client@1.0.0
