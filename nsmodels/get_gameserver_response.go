// Code generated by go-swagger; DO NOT EDIT.

package nsmodels

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// GetGameserverResponse get gameserver response
//
// swagger:model GetGameserverResponse
type GetGameserverResponse struct {

	// gameserver
	Gameserver *Gameserver `json:"gameserver,omitempty"`

	// message
	Message string `json:"message,omitempty"`
}

// Validate validates this get gameserver response
func (m *GetGameserverResponse) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateGameserver(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *GetGameserverResponse) validateGameserver(formats strfmt.Registry) error {
	if swag.IsZero(m.Gameserver) { // not required
		return nil
	}

	if m.Gameserver != nil {
		if err := m.Gameserver.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("gameserver")
			}
			return err
		}
	}

	return nil
}

// ContextValidate validate this get gameserver response based on the context it is used
func (m *GetGameserverResponse) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	var res []error

	if err := m.contextValidateGameserver(ctx, formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *GetGameserverResponse) contextValidateGameserver(ctx context.Context, formats strfmt.Registry) error {

	if m.Gameserver != nil {
		if err := m.Gameserver.ContextValidate(ctx, formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("gameserver")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *GetGameserverResponse) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *GetGameserverResponse) UnmarshalBinary(b []byte) error {
	var res GetGameserverResponse
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
