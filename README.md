# Nitrado Service V3 Client

## Client Generation using go-swagger
### Get Latest Swagger Doc
```
rm swagger.yml
cp ~/go/src/gitlab.com/BIC_Dev/nitrado-service-v3/docs/swagger.yml .
```

### Create Client and Models
```
swagger generate client -A nscv3 -c nsclient -m nsmodels -f ~/go/src/gitlab.com/BIC_Dev/nitrado-service-v3-client/docs/swagger.yml -t ~/go/src/gitlab.com/BIC_Dev/nitrado-service-v3-client
```

### Init Go Models and Tidy
```
go mod init
go mod tidy
```

### Speed up package pickup
```
GOPROXY=https://proxy.golang.org GO111MODULE=on go get gitlab.com/BIC_Dev/nitrado-service-v3-client@vX.X.X
curl https://sum.golang.org/lookup/gitlab.com/BIC_Dev/nitrado-service-v3-client@X.X.X
```